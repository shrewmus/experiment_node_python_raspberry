import time
import binascii
import logging
from payment.msgutils import MessageGenerator
from payment.msgutils import ascii_num
import os
from config import FotomatConfig

dir = os.path.dirname(os.path.realpath(__file__))
logger = logging.getLogger('myapp')
hdlr = logging.FileHandler(dir + '/myapp.log')
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)
config = FotomatConfig()
config.read()

opnum = config.get_value('opnum')

print('opnum',opnum, opnum + 1)

message_generator = MessageGenerator()
message_generator.current_operation_number = opnum
# print('amount')
# message_generator.gen_AMOUNT_msg()
# msg = message_generator.gen_VRP_msg()
# print('----')
# print(msg)
# print('----')
# cmd = message_generator.gen_cmd(msg)
# print('----')
# print(cmd)

cmd2 = message_generator.gen_VRP()

logger.info(cmd2)

print('cmd2', cmd2)

encmcd = binascii.a2b_hex(cmd2)

print('enc2md', encmcd)

# c = 237025977136523702055991
# c = str(c)
# length = len(c) // 2
#
# for i in range(length):
#     d = c[i*2: i*2 + 2]
#     d = ascii_num(d)
#     d = ((chr(d)).encode()).decode()
#     print(d)



# cmd = message_generator.gen_VRP()
# print('cmd')
# print(cmd)
# encoded_cmd = binascii.a2b_hex(cmd)
# print('ENCODED', encoded_cmd)

# dd = message_generator.join_messages_bin()
# print(dd)
# enc = binascii.a2b_hex(cmd)
# print('encoded cmd')
# print(enc)

# b'
# \x00
# \x0e
# \x96
# \xfb
#
# x01\
# x03
# VRP
#
# \x04
# \x02\x01\x00
#
# \x03\
# x01
# \x00'


# 00
# 0E
# 96
# FB
#
# 01
# 03
# 565250
#
# 04
# 02
# 0100
#
# 03
# 01
# 00

import json
import os
import logging

dir = os.path.dirname(os.path.realpath(__file__))
logger = logging.getLogger('vendotek')
hdlr = logging.FileHandler(dir + '/vendotek.log')
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)

class FotomatConfig:
    data = {}
    dir = os.path.dirname(__file__)

    file_name = dir + '/config.json'

    def save(self):
        with open(self.file_name, 'w') as cfg_file:
            json.dump(self.data, cfg_file)

    def read(self):
        with open(self.file_name) as cfg_file:
            self.data = json.load(cfg_file)
        # logger.info('config data', self.data)

    def set_value(self, key, value, target=None):
        if target:
            target[key] = value
        else:
            self.data[key] = value
        # logger.info('try to store val', key, value)


    def get_value(self, key, target=None):
        if target:
            return target[key]
        else:
            return self.data[key]

import Pyro4
import time
import serial
import os


@Pyro4.expose
class SPortWorker(object):
    DEF_STATE = "~P00000"
    ser = serial.Serial('/dev/ttyACM0')

    def __init__(self):
       time.sleep(1)
       self.ser.write(self.DEF_STATE.encode())

    def send_power(self):
        print('-- power on --')
        self.ser.write("~P00001".encode())
        time.sleep(2)
        print('-- power off --')
        self.ser.write(self.DEF_STATE.encode())

    def send_custom(self, custom):
        cmd = '~P' + custom
        print('-- set gpio state ' + cmd + ' --')
        self.ser.write(cmd.encode())

    def send_def(self):
        print('-- set gpio default state ' + self.DEF_STATE + ' --')
        self.ser.write(self.DEF_STATE.encode())

    def change_queue(self):
        # top open
        self.ser.write("~P01000".encode())
        time.sleep(1)
        self.ser.write("~P11000".encode())
        time.sleep(1)
        self.ser.write("~P01000".encode())
        time.sleep(1)
        self.ser.write("~P00000".encode())
        time.sleep(1)
        print('-- ')

    def wifi_list(self):
        response = os.popen('sudo iwlist scan | grep ESSID').read()
        return response

    def ip_set(self):
        os.open('sudo ifconfig wlp2s0 192.168.0.1')
    # ser = serial.Serial('/dev/ttyUSB0')
    # ser.break_condition = True
    #
    # ser_back = serial.Serial('/dev/ttyUSB1')
    # ser_back.break_condition = True
    #
    # def send_power(self):
    #     self.ser.break_condition = False
    #     time.sleep(2)
    #     self.ser.break_condition = True
    #     return 'power:ready'
    #
    # def send_back(self):
    #     self.ser_back.break_condition = False
    #     time.sleep(2)
    #     self.ser_back.break_condition = True
    #     return 'back:ready'




daemon = Pyro4.Daemon()
uri = daemon.register(SPortWorker)

print('--- ready daemon ---', uri)
dirname = os.path.dirname(__file__)
uri_file_path = os.path.join(dirname, 'daemon.uri.txt')
uri_file = open(uri_file_path, 'w')
uri_file.write(str(uri))
uri_file.close()

daemon.requestLoop()

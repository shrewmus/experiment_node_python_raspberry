import Pyro4
import os
import sys

dirname = os.path.dirname(__file__)
uri_file_path = os.path.join(dirname, 'daemon.uri.txt')
uri_file = open(uri_file_path, 'r')
uri = uri_file.read()
sport_server = Pyro4.Proxy(uri)

# print(sport_server.send_power())

if sys.argv[1] == 'power':
    print('-- power --')
    print(sport_server.send_power())

if sys.argv[1] == 'def':
    print(sport_server.send_def())

if sys.argv[1] == 'cmd':
    print(sport_server.send_custom(sys.argv[2]))

if sys.argv[1] == 'ten':
    print(sport_server.change_queue())

if sys.argv[1] == 'wifi':
    print(sport_server.wifi_list())

if sys.argv[1] == 'ipset':
    sport_server.ip_set()

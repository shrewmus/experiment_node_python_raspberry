import pygame
import time
import subprocess
import serial
import logging

print('hello from ', __name__)

try:
    import RPi.GPIO as GPIO
except ImportError:
    pass

import instax
import os
import sys
import logging

clock = pygame.time.Clock()
net_name = "INSTAX-"

is_ping = False
is_searching = False
is_print = False
is_found = False
logger = logging.getLogger('printer logger')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('printer.log')
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

class TimeDbg:
    start_time = None
    op_time = None

    def time_delta(self, message):
        tdelta = time.time() - self.start_time
        logger.info(message + ' ' + str(tdelta))
        print(message, time.time() - self.start_time)



def get_instax():
    try:
        return instax.SP2(ip='192.168.0.251', port=8080, timeout=15, pinCode=1111)
    except:
        return None


def printProgress(count, total, status=''):
    logging.info(status)
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))
    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)
    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
    sys.stdout.flush()

def printer_data():
    my_instax = get_instax()
    info = my_instax.getPrinterInformation()
    # info = {
    #     'model': 'Test model',
    #     'version': {
    #         'firmware': '10.1'
    #     },
    #     'battery': '4',
    #     'printCount': 1,
    #     'count': 66
    # }
    printPrinterInfo(info)


def logAndPrint(message):
    print(message)
    # logging.info(message)


def printPrinterInfo(info):
    """ Log Printer information"""
    logAndPrint("Model: %s" % info['model'])
    logAndPrint("Firmware: %s" % info['version']['firmware'])
    logAndPrint("Battery State: %s" % info['battery'])
    logAndPrint("Prints Remaining: %d" % info['printCount'])
    logAndPrint("Total Lifetime Prints: %d" % info['count'])
    logAndPrint("")

# def search_wifi(callback = None):
#     flag = True
#     start_ticks = pygame.time.get_ticks()
#     cur = 0
#
#     while flag:
#         clock.tick(30)
#         seconds = (pygame.time.get_ticks() - start_ticks) // 1000
#         if cur < seconds:
#             cur += 1
#
#         time_dbg.time_delta('---- SCAN WIFI ----')
#         # p = subprocess.Popen("sudo nmcli dev wifi rescan", stdout=subprocess.PIPE, shell=True)
#         p = subprocess.Popen("sudo iwlist wlan0 scan | grep ESSID", stdout=subprocess.PIPE, shell=True)
#         (output, err) = p.communicate()
#         # p = subprocess.Popen("nmcli -f in-use,ssid dev wifi", stdout=subprocess.PIPE, shell=True)
#         # (output, err) = p.communicate()
#
#         if net_name in str(output):
#             flag = False
#             time_dbg.time_delta('-- WIFI FOUND --')
#             on_wifi_found(callback)



def access_to_printer(callback = None):

    res = check_ping()
    time_dbg.time_delta('-- call ping 1 --')
    while not res:
        res = check_ping()
        time_dbg.time_delta('-- call ping --')
    else:
       time_dbg.time_delta('-- printer:ready --')
       if callback:
           callback()


def print_image():
    time_dbg.time_delta('---- PREPARE TO PRINT -----')
    dir = os.path.dirname(os.path.realpath(__file__))
    file_path = dir + '/img2.jpg'
    print("---------- file path %s", file_path)
    time.sleep(2)
    print('--------- SEND PRINT START ------')
    result = os.popen('instax-print ' + file_path + ' -v 2').read()
    print(result)


def check_ping():
    hostname = "192.168.0.251"
    response = os.system("ping -i 0.8 -c 1 " + hostname)
    # and then check the response...
    res = False
    if response == 0:
        pingstatus = "Network Active"
        res = True
    else:
        pingstatus = "Network Error"
        res = False

    time_dbg.time_delta('--- PING TIME ---')
    print(pingstatus)
    return res

def on_printer_found(callback=None):
    time_dbg.time_delta('--- printer ping success ---')
    # sys.stdout.flush()
    if callback:
        callback()

time_dbg = TimeDbg()
time_dbg.start_time = time.time()

if sys.argv[1] == 'wifi':
    print('-- start')
    access_to_printer()
    # access_to_printer(on_printer_found)

if sys.argv[1] == 'print':
    # search_wifi(print_image)
    print_image()
    sys.stdout.flush()

if sys.argv[1] == 'info':
    printer_data()


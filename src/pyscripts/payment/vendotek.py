import binascii
import traceback
import socket
import time
import logging
import os
import sys
from . import msgutils

print('hello from ', __name__)

dir = os.path.dirname(os.path.realpath(__file__))
logger = logging.getLogger('vendotek')
hdlr = logging.FileHandler(dir + '/vendotek.log')
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)

class VendotekCommunicator:
    '''
    Class for communicate with vendotek terminal
    '''
    sock = None
    server = None
    op_number = 0
    timeout = 15
    msg_gen = None
    is_test = False

    def __init__(self, host='192.168.1.100', port=62801):
        # todo use try to catch socket exception
        self.server = (host, port)
        # self.create_socket()
        self.msg_gen = msgutils.MessageGenerator()

    def create_socket(self, timeout=15):
        if not self.is_test:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.settimeout(timeout)

    def send_and_receive(self, command, timeout=None):
        if not timeout:
            timeout=self.timeout
        print('send_and_receive timeout', timeout)    
        self.create_socket(timeout=timeout)
        time.sleep(1)
        # todo use try to catch socket exception
        if self.sock:
            self.sock.connect(self.server)
        time.sleep(1)
        # print('COMMAND', command)
        logger.info('COMMAND: ')
        logger.info(command)
        encoded_cmd = binascii.a2b_hex(command)
        # encoded_cmd = bytes.fromhex(command)
        logger.info('ENCODED: ' + command)
        # print('ENCODED', encoded_cmd)
        if self.is_test:
            return self.imitate_send(encoded_cmd)
        else:
            return self.socket_send(encoded_cmd)


    def imitate_send(self, command):
        msggen = msgutils.MessageGenerator()
        print('TEST COMMAND', command)
        command = binascii.b2a_hex(command)
        command=command.decode()
        send_cmd = msggen.decode_cmd(command)
        msg_ascii = send_cmd.get_ascii_message()
        if msg_ascii == 'IDL':
            # msggen.direction
            return msggen.gen_IDL()
        elif msg_ascii == 'VRP':
            return msggen.gen_VRP()

    def socket_send(self, command):
        self.sock.send(command)
        data = b''
        current_len = 0
        try:
            tmp = self.sock.recv(1)
            data += tmp
            while tmp:
                tmp = self.sock.recv(1)
                data += tmp
                if len(data) == 2:
                    print('data cur', data, len(data))
                    current_len = int.from_bytes(data, byteorder='big')
                if 0 < current_len == len(data) - 2:
                    print('END READ!!', current_len, len(data), data)
                    break
        except Exception as e:
            print('Error', str(e))
            logger.error('SEND AND RECEIVE ERROR: ' + str(e))
        finally:
            self.sock.close()
            return data


    # def open_and_connect_sta(self, command, stop_message):
    #     #
    #     self.sock.connect(self.server)
    #     time.sleep(1)
    #     self.send(command)
    #
    # def receive(self):
    #     data = b''
    #     tmp = self.sock.recv(1024)
    #     while tmp:
    #         data += tmp
    #         tmp = self.sock.recv(1024)
    #     return data


class VendotekOperations:
    communicator = None
    current_state = None
    message_generator = None
    config = None
    is_test = False

    def __init__(self, configuration):
        self.config = configuration
        self.communicator = VendotekCommunicator()
        self.message_generator = msgutils.MessageGenerator()
        price = self.config.get_value('amount')
        self.message_generator.def_amount = int(price)
        self.message_generator.current_operation_number = int(self.config.get_value('opnum'))
        # print('---- opnum', self.message_generator.current_operation_number)

    def set_test(self, val):
        self.is_test = val
        self.communicator.is_test = val

    def send_idl(self):
        data = self.communicator.send_and_receive(self.message_generator.gen_IDL())
        hex_data = binascii.b2a_hex(data)
        print('hex1', hex_data)
        hex_data = hex_data.decode()
        print('hex2', hex_data)
        cmd = self.message_generator.decode_cmd(hex_data)

        return cmd

    # def wait_for_start(self):
    #     while self.current_state != self.message_generator.msg_start:
    #         data = self.send_idl()
    #         decoded_cmd = self.message_generator.decode_cmd(data)
    #         idx = 0
    #         for tag in decoded_cmd.tags:
    #             if tag == self.message_generator.tags[self.message_generator.tag_msg]:
    #                 break
    #             idx += 1
    #         currmessage = decoded_cmd.messages[idx]
    #         if currmessage == self.message_generator.msg_start:
    #             self.current_state = self.message_generator.msg_start
    #             self.cur_cmd = decoded_cmd
    #             break
    #         else:
    #             # currmessage == self.message_generator.msg_idle:
    #             self.current_state = self.message_generator.msg_idle
    #     else:
    #         self.send_vrp()

    def send_vrp(self):
        vrp = self.message_generator.gen_VRP()
        print('--- vrp before send ---')
        print(vrp)
        data = self.communicator.send_and_receive(vrp, timeout=self.communicator.timeout)
        print('resp vrp', data)
        return data


    def send_fin(self):
        data = self.communicator.send_and_receive(self.message_generator.gen_FIN())
        print('resp fin', data)
        return data

    def toggle_test(self):
        self.communicator.is_test = not self.communicator.is_test

    def pay(self, isTest=False, testRes=True):
        # self.communicator.is_test = True
        print('send PAY IDL')
        if isTest:
            # print('sleep')
            # time.sleep(3)
            # print('nosleep')
            dat ='49505149'
            return
        resp = self.send_idl()
        print('-- end IDL --')
        # resp = self.decode_response(resp)
        time.sleep(1)
        fin_amount = 0
        try:
            print('send PAY VRP')
            self.communicator.timeout = 120
            resp = self.send_vrp()
            print('decode PAY vrp response')
            resp = self.decode_response(resp)
            print('d-msg', resp.get_ascii_message())
            print('d-opn', resp.get_operation_number())
            fin_amount = resp.get_amount_value()
            # print('d1-finamount', fin_amount)
            # fin_amount = ''  + fin_amount
            # fin_amount = msgutils.str_to_ascii(fin_amount)
            print('d-finamount', fin_amount)
            resp.print_decoded()
            # print('--- FIN_AM ---', fin_amount, int(fin_amount), int(chr(fin_amount)))
        except:
            # print('PAY exception', sys.exc_info()[0])
            traceback.print_exc()
        finally:
            print('send finally PAY')
            self.communicator.timeout = .2
            self.send_fin()
            self.incr_opnum()
            return int(fin_amount) > 0

    def incr_opnum(self):
        new_num = self.message_generator.current_operation_number + 1
        self.config.set_value('opnum', new_num)
        self.config.save()

    def decode_response(self, resp):
        resp = self.prep_for_decode(resp)
        print('decode resp', resp)
        resp = self.message_generator.decode_cmd(resp)
        self.get_params_from_response(resp)
        return resp

    def prep_for_decode(self, bindata):
        bindata = binascii.b2a_hex(bindata)
        bindata = bindata.decode()
        return bindata

    def get_params_from_response(self, cmd):
        opnum = cmd.get_operation_number()
        if opnum > self.message_generator.current_operation_number:
            self.message_generator.current_operation_number = opnum
        op_timeout = cmd.get_timeout_val()
        if op_timeout > 0:
            self.communicator.timeout = op_timeout

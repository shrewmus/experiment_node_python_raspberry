import binascii

print('hello from ', __name__)

def hexify(number):
    if number < 0:
        raise ValueError('Invalid number to hexify - must be positive')

    result = hex(int(number)).replace('0x', '').upper()
    if divmod(len(result), 2)[1] == 1:
        result = '0{}'.format(result)

    return result


def ascii_num(number):
    number = str(number)
    ret = ''
    for idx in range(len(number)):
        ret += '0' + number[idx]
    return ret


def str_to_ascii(string):
    res = ''
    length = len(string)
    if length % 2 != 0:
        raise Exception('this is odd')
    curpos = 0
    while curpos < length:
        dig = string[curpos: curpos+2]
        dig = chr(int(dig))
        res += dig
        curpos += 2
    return res


class DecodedCommand:
    cmdlen = 0
    descriptor = None
    tags = []
    messages = []
    msggen = None

    def __init__(self):
        self.msggen = MessageGenerator()

    def get_ascii_message(self):
        return self.get_tag_value('', self.msggen.tag_msg)

    def get_amount_value(self):
        return self.get_tag_value(0, self.msggen.tag_amount)

    def get_timeout_val(self):
        return int(self.get_tag_value(0, self.msggen.tag_operation_timeout))

    def get_operation_number(self):
        return int(self.get_tag_value(0, self.msggen.tag_opnum))


    def get_tag_value(self,val, tag):
        try:
            print('decode', tag)
            idx = self.tags.index(self.msggen.tags[tag])
            print('decode idx', idx, len(self.messages))
            val = self.messages[idx]
        finally:
            print('decode val', val)
            return val

    def print_decoded(self):
        print('--- DECODED CMD -------------')
        print('----------- tags ------------')
        strval = ' : '.join(map(str, self.tags))
        print(strval)
        print('--------- msg values -------')
        strval = ' : '.join(map(str, self.messages))
        print(strval)
        print('------- END -------------')

class Encoder:

    def encodeOneByteInt(self, numberToEncode):
        oneByteInt = bytearray()
        oneByteInt.append((numberToEncode >> 0) & 0xFF)
        return oneByteInt

    def encodeTwoByteInt(self, numberToEncode):
        """Encode a Two Byte Integer."""
        twoByteInt = bytearray()
        twoByteInt.append((numberToEncode >> 8) & 0xFF)  # B1
        twoByteInt.append((numberToEncode >> 0) & 0xFF)  # B2
        return twoByteInt


class MessageGenerator:
    is_debug = False
    config = None
    def_amount = 1
    amount = str(def_amount) + '00'
    tags = {
        'msg_tag': 0x01,
        'op_num_tag': 0x03,
        'amount_tag': 0x04,
        'keepalive_tag': 0x05,
        'operation_timeout_tag': 0x06
    }
    taglen = {
        'msg_tag': 3,
        'op_num_tag': 8,
        'amount_tag': 12,
        'keepalive_tag': 3,
        'operation_timeout_tag': 3
    }

    tag_msg = 'msg_tag'
    tag_opnum = 'op_num_tag'
    tag_amount = 'amount_tag'
    tag_keepalive = 'keepalive_tag'
    tag_operation_timeout = 'operation_timeout_tag'

    msg_idle = 'IDL'
    msg_disabled = 'DIS'
    msg_start = 'STA'
    msg_vend_request_positive = 'VRP'
    msg_finalization = 'FIN'

    descr_vmc = 0x96FB
    descr_pos = 0x97FB

    encoder = Encoder()

    current_operation_number = 0
    dir_vm_to_pos = 'VM->POS'
    dir_pos_to_vm = 'POS->VM'
    direction = dir_vm_to_pos

    messages = []

    def clear_messages(self):
        self.messages = []

    def add_message(self, msg):
        self.messages.append(msg)

    # def join_messages_bin(self):
    #     bar = bytearray()
    #     for msg in self.messages:
    #         bar.append(msg)
    #     print(bar)

    def join_messages(self):
        ret_str = ''
        for msg in self.messages:
            ret_str += msg
        return ret_str

    # def join_bin(self):
    #     ret = b''
    #     for msg in self.messages:
    #         ret += msg
    #     return ret

    def gen_message(self, msg, tag):
        message = msg
        if type(msg) is int:
            message = str(message)
        tag = hexify(self.tags[tag])
        if self.is_debug:
            print('tag', tag)
            print('msg', msg)
        if tag == self.tag_amount:
            print('message', msg)
        # if tag == self.tag_opnum:
        #     message = 0
        length = hexify(len(message))
        msg = binascii.hexlify(message.encode())
        msg = msg.decode()
        if self.is_debug:
            print('len', length)
        # todo detect conversion method by tag
        # try:
        #     message = int(message)
        #     message = ascii_num(message)
        #     length = hexify(len(message) / 2)
        #     msg = message
        # except:
        #     message = msg
        #     length = hexify(len(message))
        #     msg = binascii.hexlify(message.encode())
        #     msg = msg.decode()
        tlg_string =tag + length + msg
        return tlg_string

    def gen_cmd(self, msg):

        cmd = hexify(self.descr_vmc)
        cmd = cmd + msg
        print(cmd)
        cmd = self.encode_len(len(cmd) // 2) + cmd
        return cmd

    def encode_len(self, number):
        number = self.encoder.encodeTwoByteInt(number)
        res = ''
        for bt in number:
            res = res + hexify(bt)
        return res

    def gen_IDL_msg(self):
        self.clear_messages()
        self.add_message(self.gen_message(self.msg_idle, self.tag_msg))
        self.gen_OPNUM_msg()
        return self.join_messages()

    def gen_IDL(self):
        return self.gen_cmd(self.gen_IDL_msg())

    def gen_VRP_msg(self):
        self.clear_messages()
        self.add_message(self.gen_message(self.msg_vend_request_positive, self.tag_msg))
        self.gen_AMOUNT_msg()
        self.gen_OPNUM_msg()
        return self.join_messages()

    def gen_STA_msg(self):
        self.clear_messages()
        self.add_message(self.gen_message(self.msg_start, self.tag_msg))
        self.gen_OPNUM_msg()
        self.gen_AMOUNT_msg()
        return self.join_messages()

    def gen_STA(self):
        return self.gen_cmd(self.gen_STA_msg())

    def gen_VRP(self):
        return self.gen_cmd(self.gen_VRP_msg())

    def gen_FIN_msg(self):
        self.clear_messages()
        self.add_message(self.gen_message(self.msg_finalization, self.tag_msg))
        self.gen_OPNUM_msg()
        return self.join_messages()

    def gen_FIN(self):
        return self.gen_cmd(self.gen_FIN_msg())

    def gen_OPNUM_msg(self):
        self.add_message(self.gen_message(self.current_operation_number, self.tag_opnum))

    def gen_AMOUNT_msg(self):
        self.add_message(self.gen_message(self.amount, self.tag_amount))

    def decode_msg(self, msg, decoded_cmd):
        # assume that tag
        tag = msg[0:2]
        decoded_cmd.tags.append(int(tag))
        msg_value = binascii.unhexlify(msg[4:])
        if int(tag, 16) == self.tags[self.tag_msg]:
            msg_value = msg_value.decode('ascii')
        # else:
        # msg_value = int(msg[4:], 16)

        decoded_cmd.messages.append(msg_value)
        print('tag', tag, len(decoded_cmd.tags))
        print('msgval', msg, msg_value)

    def decode_cmd(self, cmd):
        if self.is_debug:
            print('Command to decode', cmd)
        # 000a96fb010349444c030130
        decoded_cmd = DecodedCommand()
        # first two bytes (4 chars)
        if len(cmd) == 0:
            return decoded_cmd
        decoded_cmd.cmdlen = int(cmd[0:4], 16)

        # descriptor another 2 bytes
        decoded_cmd.desc = cmd[4:8]
        msg_marker = 8
        # first message length after tag byte (2 chars)
        msglen = int(cmd[10:12], 16)
        # if message length + one byte for tag + one byte for message length + two byte for length
        # is equal to cmdlen - so we have one message
        is_one_msg = msglen + 4 == decoded_cmd.cmdlen
        parsed_msg_len = msglen + 2
        is_last_message = False
        cur_msg_len = 0
        if is_one_msg:
            self.decode_msg(cmd[8:], decoded_cmd)
        else:
            while not is_last_message:
                cur_msg_len = int(cmd[msg_marker + 2: msg_marker + 4], 16)
                msg = cmd[msg_marker: msg_marker + 4 + cur_msg_len * 2]
                self.decode_msg(msg, decoded_cmd)
                msg_marker += cur_msg_len * 2 + 4
                is_last_message = (msg_marker - 2) / 2 >= decoded_cmd.cmdlen
            # else:
            #     if self.is_debug:
            #         print('last message')
            #     cur_msg_len = int(cmd[msg_marker + 2: msg_marker + 4], 16)
            #     msg = cmd[msg_marker: msg_marker+4+cur_msg_len*2]
            #     self.decode_msg(msg, decoded_cmd)
        return decoded_cmd

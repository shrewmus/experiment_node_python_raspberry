import sys
from payment.vendotek import VendotekOperations
from config import FotomatConfig

print('hello from ', __name__)

config = FotomatConfig()
config.read()

vendotek = VendotekOperations(config)

def call_payment():
    # reset()
    if vendotek.pay(isTest=False, testRes=True):
        return 'vend:positive'
    else:
        return 'vend:negative'

def reset():
    vendotek.send_idl()

if sys.argv[1] == 'reset':
    reset()

if sys.argv[1] == 'start':
    print('start')
    print(call_payment())

sys.stdout.flush()

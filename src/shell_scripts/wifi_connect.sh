#!/bin/bash
DEV=wlp2s0

if [[ -f /run/dhcpcd-$DEV.pid ]]
then
	kill -QUIT $(cat /run/dhcpcd-$DEV.pid)
fi

ifconfig $DEV down
iw dev $DEV set type managed
ifconfig $DEV up
sleep 2

wpa_supplicant -D nl80211 -i$DEV -c /etc/wpa_supplicant/wpa_supplicant.conf
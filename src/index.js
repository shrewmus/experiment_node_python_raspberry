'user strict';
/**
 * @file index.js
 * @author shrewmus (shrewmus@yandex.ru contact@shrewmus.name)
 * Date: 9/13/18
 * (c) 2018
 */

const express = require('express');
const cors = require('cors');
const app = express();
const exec = require('child_process').spawn;
const path = require('path').dirname(require.main.filename);
const bodyParser = require('body-parser');
const fs = require('fs');
const request = require('request');
const util = require('util');
const Jimp = require('jimp');
const sudoCall = require('sudo-js');
const nodeconf = require('./nodeconf');
sudoCall.setPassword(nodeconf.sudoPass);

app.use(cors({origin: '*', preflightContinue: true}));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
// const staticPath = '/home/fotomat/fotomatwork/fotomat_ionic/www';
const staticPath = '/home/shrewmus/Projects/photomat/fotomat_ionic/www';
app.use(express.static('/home/fotomat/fotomatwork/fotomat_ionic/www'));
const printerStatus = 'off';

// const logFile = fs.createWriteStream(__dirname + '/debug.log', {flags: 'w'});
// const logStdout = process.stdout;
//
// console.log = function(d) {
//     logFile.write(util.format.apply(d) + '\n');
//     logStdout.write(util.format.apply(d) + '\n');
// };
//
// console.error = console.log;


let printCount = 10;

app.get('/', (req,res) => {
    res.sendFile('/home/fotomat/fotomatwork/fotomat_ionic/www/index.html');
});

app.get('/update-config', (req, res) => {
    const postData = {
        "id":  1
    };
    const url = 'http://' + nodeconf.adminHost + '/devices/get-device-config';
    request.post(url, {json: postData}, (error, result, body) => {
        const strResponse = JSON.stringify(body);
        fs.writeFile(__dirname + '/device.conf', strResponse, (err) => {
            res.send(strResponse);
            res.end();
        })
    })
});

app.get('/get-config', (req, res) => {
    console.log('get config start');
   fs.readFile(__dirname + '/device.conf', (err, data) => {
       if (err) {
           console.log('err', err);
       }
       console.log('read complete send');
       res.send(data);
       res.end();
   })
});


app.get('/current-counts', (req, res) => {
    res.send({'result': 'ok', cnt: printCount}).end();
});

//<editor-fold desc="Calls to printer via python serial caller">
app.get('/printer-def', (req, res) => {
    pythonSerialInterface('def');
    res.send({'result': 'ok'}).end();
});

app.get('/printer-power', (req, res) => {
    pythonSerialInterface('power');
    res.send({'result': 'ok'}).end();
});

app.get('/printer-cmd', (req, res) => {
    pythonSerialInterface('cmd', req.query.cmd);
    res.send({'result': 'ok'}).end();
});

app.get('/print-change', (req, res) => {
    pythonSerialInterface('ten');
    res.send({'result': 'ok'}).end();
});

function pythonSerialInterface(type, cmd) {

    const defArgs = [path + '/pyscripts/sportdaemon/pserial_caller.py', type];
    if (cmd) {
        defArgs.push(cmd);
    }

    const childProcess = exec(
        'python3',
        defArgs,
        {shell: true}
    );

    childProcess.stdout.on('data', (data) => {
        const resp = data.toString('utf8');
        console.log('=== data from printer ==', resp);
    });
}
//</editor-fold>

function callPythonPrinterWorks(type, data = null, callback = null, timeout = 7000, isKill = false) {
    const outputData = [];
    const arguments = [path + '/pyscripts/printer_works.py', type];
    if (null !== data) {
        arguments.push(data);
    }
    const childProcess = exec(
        'python3',
        arguments,
        {shell:true}
    );
    childProcess.stdout.on('data', (data) => {
        const resp = data.toString('utf8');
        console.log('=== data from printer ===', resp);
        outputData.push(resp);
        if (resp.includes('Prints Remaining')) {
            const pattern = /Remaining\:\s(\d+)/;
            const number = pattern.exec(resp);
            printCount = parseInt(number[1]);
        }
    });

    setTimeout(() => {
        if (isKill) {
            childProcess.kill()
        }
        if (callback) {
            callback(outputData)
        }
    }, timeout)
}

//<editor-fold desc="Printer status">
app.get('/printer-status', (req, res) => {
    callPythonPrinterWorks('info', null, (data) => {
        res.send({'result': 'ok', data, printCount}).end();
    })
});
//</editor-fold>


//<editor-fold desc="Printer await">
app.get('/printer-await', (req, res) => {

    callPythonPrinterWorks('wifi', null, (data) => {
        console.log('data::', data);
        let output = 'not';
        if (data.join(',').includes('printer:ready')) {
            output = 'ready';
            callPythonPrinterWorks('ipset', null, (data) => {
                console.log('set wifi ip res', data);
            })
        }
        res.send({result: 'ok', printer: output})
    }, 7000, true)
});
//</editor-fold>


// app.get('/power', (req, res) => {
//     const t1 = new Date().getTime();
//     console.log('=== call power ===');
//
//     const childProcess = exec(
//         'python3',
//         [path + '/pyscripts/sportdaemon/pserial_caller.py', 'power'],
//         {shell: true});
//
//     childProcess.stdout.on('data', (data) => {
//         const resp = data.toString('utf8');
//         const t2 = new Date().getTime();
//         console.log('=== wifi connect output ===', resp, t2 - t1);
//
//         const secondChild = exec(
//             'python3',
//             [path + '/pyscrips/printer_works.py', 'wifi'],
//             {shell: true}
//         );
//
//         secondChild.stdout.on('data', (data) => {
//             const resp = data.toString('utf8');
//             const t3 = new Date().getTime();
//             console.log('=== ping search output ===', resp, t3 - t1);
//         });
//     });
//     res.send({'result': 'ok'}).end();
// });

app.get('/wifi-check', (req, res) => {
    console.log('path', path);
    const subpocess = exec('python3', [path + '/pyscripts/sportdaemon/pserial_caller.py', 'wifi']);
    const respdata = [];
    subpocess.stdout.on('data', (data) => {
        const resp = data.toString('utf8');
        console.log('== printer wifi check output ===', resp);
        respdata.push(resp);
    });
    subpocess.stderr.on('data', (data) => {
        console.log('data', data.toString('utf8'));
    });

    setTimeout(() => {
        let out = 'no';
        if (respdata.join(',').includes('INSTAX-01147484')) {
            out = 'yes';
        }
        res.send({result: 'ok', data: out}).end();
    }, 3000)

});

// app.get('/wifi', (req, res) => {
//     const subprocess = exec('python3', [path + '/pyscripts/printer_works.py', 'wifi']);
//     subprocess.stdout.on('data', (data) => {
//         const resp = data.toString('utf8');
//         console.log('=== printer wifi output ===', resp);
//         let response = 'off';
//         if (resp.includes('1')) {
//             response = 'on';
//         }
//         res.send({result:'ok', status: response}).end();
//     })
// });

app.get('/wifion', (req, res) => {
    wifiOn();
    res.send({result: 'ok'}).end();
});

app.get('/wifioff', (req, res) => {
    wifiOff();
    res.send({return: 'ok'}).end();
});

function wifiOn() {

    console.log('=== call wifi connect ===');
    sudoCall.exec('bash', [path + '/shell_scripts/wifi_on.sh'], (err, pid, result) => {
        console.log('wifi on call:', result);
    });
}

function wifiOff() {
    console.log('=== call wifi disconnect ===');
    sudoCall.exec('bash', [path + '/shell_scripts/wifi_off.sh'], (err, pid, result) => {
        console.log('wifi off call:', result);
    });
}

app.get('/paymentreset', (req, res) => {
    console.log('=== call payment reset ===');
    const process = exec('python3', [path + '/pyscripts/terminal_works.py', 'reset']);
    process.stdout.on('data', (data) => {
        const resp = data.toString('utf8');
        console.log('=== payment reset output ===', resp);
    });
    res.send({'result': 'ok'}).end()
});

app.get('/payment', (req, res) => {
    console.log('=== call payment request ===');
    const process = exec('python3', [path + '/pyscripts/terminal_works.py', 'start']);
    process.stdout.on('data', (data) => {
        const resp = data.toString('utf8');
        console.log('=== payment output ===', resp);
        let result = 'unknown';
        if (resp.includes('vend:positive')) {
            result = 'positive';
        }
        if (resp.includes('vend:negative')) {
            result = 'negative';
        }

        res.send({'result': 'ok', 'data': result, 'prstate': 'unknown'}).end()
    });
    process.stderr.on('data', (err) => {
        err = err.toString('utf8');
        console.log('=== payment err ===', err);
    })
});

app.get('/ping', (req, res) => {
    console.log('=== call printer ping ===');
    const process = exec('python3', [path + '/pyscripts/printer_works', 'ping']);
    const resps = [];
    let result = 'no';
    process.stdout.on('data', (data) => {
        const resp = data.toString('utf8');
        console.log('=== ping output ===', resp);
        if (resp.includes('Network Active')) {
            result = 'yes';
        }
        resps.push(resp);
    });

    setTimeout(() => {
        res.send({'result': 'ok', ping: result, data: resps.join(',')});
    }, 3000);
});



app.get('/crop', (req, res) => {
    handleImage()
});

app.post('/imgsave', (req, res) => {
    const callback = () => {
        res.send({result: 'ok'}).end();
    };
    saveImage(req.body.img, callback);
});

function saveImage(base64data, callback) {
    console.log('=== save image file ===');
    fs.writeFile(path + '/pyscripts/img-temp.jpg', base64data, 'base64', (err) => {
        console.log(' === end image write ===');
        if (err) {
            console.log('ERR', err.message);
        }
        callback();
    })
}

app.get('/print-test', (req, res) => {
    callToPrint(res);
});

app.post('/print', (req, res) => {

    console.log('=== call print ===');
    fs.writeFile(path + '/pyscripts/img.jpg', req.body.img, 'base64', (err) => {
        console.log('=== end write ===');
        if (err) {
            console.log('ERR', err);
        }
        handleImage().then(() => {
            console.log('=== image handle end ===');
            setTimeout(() => {
                callToPrint(res);
            }, 5000);
        })
    });
});


function setGeneralTimeout() {
    return setTimeout(() => {

        console.log('=== print task killed by timeout ===');

        try {
            process.kill();
        } catch (e) {
            console.log('kill err', e.message);
        }
        // return res.send({'result': 'error'}).end()
    }, 55000);
}

function printDate() {
    const date = new Date();
    console.log('=== TIME ===', [date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds()].join(':'));
}

function callToPrint(res) {

    console.log('== go to print ==');

    const process = exec('python3', [path + '/pyscripts/printer_works.py', 'print']);
    let printTimeout;
    let generalTimeout = setGeneralTimeout();

    const resp2 = [];

    process.stdout.on('data', (data) => {
        const resp = data.toString('utf8');

        console.log('=== PRINTER RESP ===', resp);


        resp2.push(resp);

        clearTimeout(generalTimeout);

        if (resp.includes('printer:ready')) {
            printDate();
            clearTimeout(generalTimeout);
            generalTimeout = setGeneralTimeout();
        }

        if (resp.includes('Instax Printer Python Client')) {
            console.log('=== START INSTAX API ===');
            printDate()
        }

        if (resp.includes('...Connecting to instax Printer')) {
            clearTimeout(generalTimeout);
            generalTimeout = setGeneralTimeout();
        }

        if (resp.includes('Checking status of print')) {
            if (generalTimeout) {
                clearTimeout(generalTimeout);
                console.log('=== clear general ===');
            }
            if (printTimeout) {
                clearTimeout(printTimeout);
                console.log('=== clear print ===');
            }
            console.log('=== [DATA PRINTED BUT NOT PRINTER STATUS] ===');
            printDate();
            printTimeout = setTimeout(() => {
                printDate();
                console.log('=== kill print ok ===');
                process.kill();
                return res.send({'result': 'ok', data: resp2}).end()
            }, 8000)
        }

        if (resp.includes('Prints Remaining')) {
            const pattern = /Remaining\:\s(\d+)/;
            let nubmer = pattern.exec(resp);
            printCount = parseInt(nubmer[1]);
        }

        if (resp.includes('Print is complete')) {

            printDate();
            console.log('=== print is complete ===');
            clearTimeout(generalTimeout);
            clearTimeout(printTimeout);
        }

        if (resp.includes('Connecting to instax Printer')) {
            console.log('=== [DATA COLLECTED] ===');

            printTimeout = setTimeout(() => {
                printDate();
                console.log('=== kill print timeout ===');
                process.kill();
                return res.send({'result': 'error', data: resp2}).end()
            }, 35000);
        }

    });
}

function handleImage() {
    console.log('=== image handling ===');
    return Jimp.read(path + '/pyscripts/img.jpg').then((img) => {
        console.log('=== try to crop ===');
        try {
            console.log(img);
            return img.crop(0, 240, 1080, 1440)
                .resize(600, 800, Jimp.RESIZE_BEZIER)
                .write(path + '/pyscripts/img2.jpg')
        } catch (e) {
            console.log('ERR', e);
        }


    }).catch((err) => {
        console.log('=== img handing error ===', err);
    })
}

const server = app.listen(8855, () => {
    console.log('Server listening at port %s', server.address().port);
});